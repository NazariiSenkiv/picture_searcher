import './App.css';
import 'bootstrap/dist/css/bootstrap.css'

import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  useNavigate } from 'react-router-dom';

import {React, useEffect, useState} from 'react';
import NaviBar from './components/Navibar';
import SearchPage from './pages/SearchPage';
import LoginPage from './pages/LoginPage';
import RegistrationPage from './pages/RegisterPage';
import SearchResultsPage from './pages/SearchResultsPage';
import AddPicturePage from './pages/AddPicturePage'
import MyPicturesPage from './pages/MyPicturesPage';
import AdministrationPage from './pages/AdministrationPage';

function App() {

  const [userName, setUserName] = useState('');
  const [iconSrc, setIconSrc] = useState('');
  const [userId, setUserId] = useState(0);
  const [userRights, setUserRights] = useState(2);

  useEffect(() => {
    const savedUserName = JSON.parse(localStorage.getItem('userName'));
    if (savedUserName) { setUserName(savedUserName);}
  }, []);

  useEffect(() => {
    const savedUserIconSrc = JSON.parse(localStorage.getItem('iconSrc'));
    if (savedUserIconSrc) { setIconSrc(savedUserIconSrc);}
  }, []);

  useEffect(() => {
    const savedUserId = JSON.parse(localStorage.getItem('userId'));
    if (savedUserId) { setUserId(savedUserId);}
  }, []);

  useEffect(() => {
    const savedUserRights = JSON.parse(localStorage.getItem('userRights'));
    if (savedUserRights) { setUserRights(savedUserRights);}
  }, []);

  const clearUserData = function() {
    setUserName('');
    setIconSrc('');
    setUserId(0);
    setUserRights(2);
  }

  const userDataSetters = {setUserName, setIconSrc, setUserId, setUserRights, clearUserData};
  const userData = {userName, iconSrc, userId, userRights};

  return (
    <div className="App">
      <Router>
        <NaviBar userData={userData} setters={userDataSetters}/>
        <Routes>
          <Route path='/' element={<SearchPage/>}/>
          <Route path='/login' element={<LoginPage setters={userDataSetters}/>}/>
          <Route path='/register' element={<RegistrationPage setters={userDataSetters}/>}/>
          <Route path='/search' element={<SearchResultsPage/>}/>
          <Route path='/add' element={<AddPicturePage userInfo={userData}/>}/>
          <Route path='/my_pictures' element={<MyPicturesPage/>}/>
          <Route path='/administration' element={<AdministrationPage/>}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
