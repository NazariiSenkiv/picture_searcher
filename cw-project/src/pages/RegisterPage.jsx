import {React, useState} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'

import styles from '../pages/LoginReg.module.css';
import { logInRequest } from '../pages/LoginPage';

const RegistrationPage = function({setters}) {

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [password, setPassword] = useState('');
	const [undernameMsg, setUndernameMsg] = useState('');
	const [underpasswordMsg, setUnderpasswordMsg] = useState('');
    const [iconURL, setIconURL] = useState('');
    const [wantImage, setWantImage] = useState(false);

	function submit(e) {
		e.preventDefault();

		let canSubmit = true;

		if(name === '') {
			setUndernameMsg("This field can't be empty!");
			canSubmit = false;
		} else {
			setUndernameMsg('');
		}

		if(password === '') {
			setUnderpasswordMsg("Password field can't be empty!");
			canSubmit = false;
		} else {
			setUnderpasswordMsg('');
		}

		if (canSubmit) {
			var serverUrl = "http://cw-backend-server/registration.php";

			var formData = new FormData();
			formData.append('name', name);
			formData.append('password', password);
			formData.append('image_src', iconURL);

			fetch(serverUrl, {
				method: "POST",
				header: {"Content-Type": 'application/x-www-form-urlencoded'},
				body: formData
			})
			.then(response => response.json())
			.then(response => {
				if (response.login_exists) {
					setUndernameMsg("This login already exists!");
				}
				else if (response.added == false) {
					alert("Server cannot add your account. Try later :)");
				}
				else {
					logInRequest(name, password)
                    .then(loginResponse => {
                        if (loginResponse.login_correct) {
                            if(loginResponse.password_correct) {
                                // setting username
                                setters.setUserName(name);
                                localStorage.setItem('userName', JSON.stringify(name));
                                // setting icon source
                                setters.setIconSrc(loginResponse.image_src);
                                localStorage.setItem('iconSrc', JSON.stringify(loginResponse.image_src));
                                // setting user id
                                setters.setUserId(loginResponse.user_id);
                                localStorage.setItem('userId', JSON.stringify(loginResponse.user_id));
                                // setting user rights
                                setters.setUserRights(loginResponse.rights);
                                localStorage.setItem('userRights', JSON.stringify(loginResponse.rights));
                    
                                console.log("(Name: "+name+", Id:"+loginResponse.user_id+", Rights: "+loginResponse.rights+")");
                                navigate('/');
                            } else {
                                setUnderpasswordMsg('Your password is incorrect!');
                            }
                        } else {
                            setUndernameMsg('There is no such user!');
                        }
                    });
				}
			});
		}
	}

	return (
		<div className={styles.form}>
            <h2 className='text-center'>Registration</h2>
			<Form>
				<Form.Group className="mb-3" controlId="formBasicName">
					<Form.Label>Your name</Form.Label>
					<Form.Control type="text" placeholder="Enter your name" onChange={(e)=>{setName(e.currentTarget.value)}}/>
					{undernameMsg===''
						? <Form.Text className="text-muted">
							This is your login name
						</Form.Text>
						: <p className={styles.red}>
							{undernameMsg}
						</p>
					}
				</Form.Group>

				<Form.Group className="mb-3" controlId="formBasicPassword">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Password" onChange={(e)=>{setPassword(e.currentTarget.value)}}/>
					{underpasswordMsg===''
						? null
						: <p className={styles.red}>
							{underpasswordMsg}
						</p>
					}
				</Form.Group>
				<Form.Group className="mb-3" controlId="formBasicCheckbox">
					<Form.Check label='Set icon url?' onChange={(e)=>{setWantImage(!wantImage)}}/>
					{
						!wantImage 
						? null
						:
						<Form.Control placeholder='Icon URL...' onChange={(e)=>{setIconURL(e.target.value)}}/>
					}
				</Form.Group>
				<div className='text-center'>
					<Button variant="secondary" type="submit" onClick={submit} >
						Log In
					</Button>
				</div>
			</Form>
		</div>
	);
}

export default RegistrationPage;