import React from "react";
import SearchBar from "../components/SearchBar";

const SearchPage = function() {
    return(
        <div>
            <h1 className="search-caption search-bar-area">Search</h1>
            <SearchBar/>
        </div>
    )
}

export default SearchPage;