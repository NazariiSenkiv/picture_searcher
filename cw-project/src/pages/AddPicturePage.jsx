import {React, useEffect, useState} from 'react'
import {Form, Button, Modal, Image} from 'react-bootstrap'

import styles from './AddPicturePage.module.css'

const AddPicturePage = function({userInfo}) {

    const [name, setName] = useState('');
    const [source, setSource] = useState('');
    const [tags, setTags] = useState('');
    const [showModal, setShowModal] = useState(false);


    const backendPageURL = 'http://cw-backend-server/addImage.php';

    function submit(e) {
        e.preventDefault();

        setShowModal(!showModal);

        var formData = new FormData();
        formData.append('name', name);
        formData.append('source', source);
        formData.append('tags', tags);
        formData.append('author_id', userInfo.userId)

        fetch(backendPageURL, {
             method: 'POST',
             header: {"Content-Type": 'application/x-www-form-urlencoded'},
             body: formData
         })
        .then(response=>response.json())
        .then(response=>{
            if(response.added == true) {
                 setShowModal(true);
            }
        });
    }

    return (
        <div>
            <div className={styles.form}>
                <Form>
                    <Form.Group className="my-3" controlId="formBasicName">
                        <Form.Label>Picture name</Form.Label>
                        <Form.Control type="text" placeholder="Enter picture name" onChange={(e)=>{setName(e.currentTarget.value)}}/>
                        <Form.Text className="text-muted">
                            This name of picture will be shown to other users
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="my-2" controlId="formBasicPassword">
                        <Form.Label>Picture source(url)</Form.Label>
                        <Form.Control type="text" placeholder="URL" onChange={(e)=>{setSource(e.currentTarget.value)}}/>  
                        <Form.Text className="text-muted">
                            URL source of picture
                        </Form.Text>  			  
                    </Form.Group>	

                    <Form.Group className="my-2" controlId="formBasicPassword">
                        <Form.Label>Tags</Form.Label>
                        <Form.Control type="text" placeholder="Enter tags" onChange={(e)=>{setTags(e.currentTarget.value)}}/>  
                        <Form.Text className="text-muted">
                            Is used for search
                        </Form.Text>  	
                    </Form.Group>	

                    <Form.Group className="my-2">
                        <Image src={source} className={styles.image}/>	
                    </Form.Group>	  

                    <div className='text-center mt-3s'>
                        <Button variant="secondary" type="submit" onClick={submit}>
                            Add
                        </Button>
                    </div>
                </Form>
            </div>
            {
            showModal ?
            <div>
                <Modal show={showModal} onHide={(e)=>{setShowModal(false);}}>
                <Modal.Header closeButton>
                    <Modal.Title>Info</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Added successfully.</p>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={(e)=>{setShowModal(false);}}>
                        Ok
                    </Button>
                </Modal.Footer>
                </Modal>
            </div>
            : null   
            }
        </div>
    );
}

export default AddPicturePage;