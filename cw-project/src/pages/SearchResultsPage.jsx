import {React, useState, useEffect} from "react";

import ImageContainer from "../components/ImageContainer";
import NoResultsMessage from "../components/NoResultsMessage";
import SearchBar from "../components/SearchBar";

const SearchResultsPage = function() {
    
    var serverUrl = "http://cw-backend-server/getImages.php";

    const [urlParams, setURLParams] = useState(new URLSearchParams(window.location.search));
    const [resp, setResp] = useState([]);
    const [prevParams, setPrevParams] = useState('');

    if (urlParams.get('q') != prevParams) {
        setPrevParams(urlParams.get('q'));

        fetch(serverUrl+'?'+urlParams)
        .then(response => response.json())
        .then(response => {
            setResp(response);
        })
    }

    return (
        <div>
            <br/>
            <SearchBar/>
            {
            resp.length > 0
            ?
            resp.map(image =>
                <div>
                    <ImageContainer imageInfo={image}/>
                </div>)
            :
            <NoResultsMessage/>
            }
        </div>
    );
}

export default SearchResultsPage;