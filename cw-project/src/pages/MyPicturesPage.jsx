import { useState } from "react";
import ImageContainer from "../components/ImageContainer";
import NoResultsMessage from "../components/NoResultsMessage";

const MyPicturesPage = function() {

    const serverURL = 'http://cw-backend-server/getUserImages.php';
    
    const [resp, setResp] = useState([]);
    const[isDrawn, setIsDrawn] = useState(false);

    if (!isDrawn) {
        setIsDrawn(true);
        
        fetch(serverURL+'?id='+JSON.parse(localStorage.getItem('userId')))
        .then(response => response.json())
        .then(response => {
            setResp(response);
        })
    }

    return (
        <div>
            {
                resp.length > 0
                ?
                resp.map(image =>
                    <div>
                        <ImageContainer imageInfo={image}/>
                    </div>)
                :
                <NoResultsMessage/>
            }
        </div>
    );
}

export default MyPicturesPage;