import {React, useEffect, useState} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'

import styles from '../pages/LoginReg.module.css'

export async function logInRequest(name, password) {
    var url = "http://cw-backend-server/login.php";

    var formData = new FormData();
    formData.append('name', name);
    formData.append('password', password);

    var res = await fetch(url, {
        method: "POST",
        header: {"Content-Type": 'application/x-www-form-urlencoded'},
        body: formData
    });
    return await res.json();
}

const LoginPage = function({setters}) {

	function submit(e)
	{
		e.preventDefault();

		let canSubmit = true;

		if (name == '') {
			setUndernameMsg('Please, enter your nickname');
			canSubmit = false;
		} else {
			setUndernameMsg('');
		}

		if(password === '') {
			setUnderpasswordMsg("Please, enter your password");
			canSubmit = false;
		} else {
			setUnderpasswordMsg('');
		}

		if (canSubmit) {
            logInRequest(name, password)
            .then(response => {
                if (response.login_correct) {
                    if(response.password_correct) {
                        // setting username
                        setters.setUserName(name);
                        localStorage.setItem('userName', JSON.stringify(name));
                        // setting icon source
                        setters.setIconSrc(response.image_src);
                        localStorage.setItem('iconSrc', JSON.stringify(response.image_src));
                        // setting user id
                        setters.setUserId(response.user_id);
                        localStorage.setItem('userId', JSON.stringify(response.user_id));
                        // setting user rights
                        setters.setUserRights(response.rights);
                        localStorage.setItem('userRights', JSON.stringify(response.rights));
            
                        console.log("(Name: "+name+", Id:"+response.user_id+", Rights: "+response.rights+")");
                        navigate('/');
                    } else {
                        setUnderpasswordMsg('Your password is incorrect!');
                    }
                } else {
                    setUndernameMsg('There is no such user!');
                }
            });
		}
	}

	const navigate = useNavigate();
	const [name, setName] = useState('');
	const [password, setPassword] = useState('');
	const [undernameMsg, setUndernameMsg] = useState('');
	const [underpasswordMsg, setUnderpasswordMsg] = useState('');

	return (
		<div className={styles.form}>
		<Form>
	  		<Form.Group className="mb-3" controlId="formBasicName">
	    		<Form.Label>Your name</Form.Label>
	    		<Form.Control type="text" placeholder="Enter your name" onChange={(e)=>{setName(e.currentTarget.value)}}/>
	      			{undernameMsg===''
	      			? <Form.Text className="text-muted">
	      				This is your login name
	      			  </Form.Text>
	      			: <p className={styles.red}>
	      				{undernameMsg}
	      			  </p>
	      			}
	  		</Form.Group>

		  	<Form.Group className="mb-2" controlId="formBasicPassword">
			  	<Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" onChange={(e)=>{setPassword(e.currentTarget.value)}}/>
					{underpasswordMsg===''
						? null
						: <p className={styles.red}>
							{underpasswordMsg}
						</p>
					}      			  
		  	</Form.Group>
			Not registered yet? <a href="" onClick={(e)=>{navigate("/register")}}>Register</a>		
            <div className='text-center mt-3s'>
                <Button variant="secondary" type="submit" onClick={submit}>
                    Log In
                </Button>
            </div>
		</Form>
		</div>
	);
}

export default LoginPage;