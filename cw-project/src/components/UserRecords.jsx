import { useState } from 'react';
import UserRecord from './UserRecord';

const backendPageURL = 'http://cw-backend-server/getUsers.php';

const UserRecords = function() {

    const[isDrawn, setIsDrawn] = useState(false);

    if (!isDrawn) {
        setIsDrawn(true);

        fetch(backendPageURL)
        .then(response => response.json())
        .then(response => {
            setUsers(response);
        });
    }

    const [users, setUsers] = useState([]);

    return (
        <div className='records-container'>
            {
                users.map(user =>
                <div>
                    <UserRecord user = {user}/>
                </div>
            )
            }
        </div>
    );
}

export default UserRecords;