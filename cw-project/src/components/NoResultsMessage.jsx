import React from "react";

const NoResultsMessage = function() {
    return (
        <div className="data-box">
            <h2 className="header">No results found</h2>
        </div>
    );
}

export default NoResultsMessage;