import React from 'react'
import {Navbar, Nav, Button, Image} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {useNavigate} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'

import logo from '../Content/web-search.svg'

const NaviBar = function({userData, setters}) {
	const navigate = useNavigate();

	const logOut = function() {
		setters.clearUserData(); 
		localStorage.clear(); 
		navigate('/');
	}

	return (
		<div>
			<Navbar collapseOnSelect expand="lg" bg="light" className="px-3 py-0 f20">
				<Navbar.Brand> 
					<Image src={logo} className="navbar-icon" onClick = {(e)=>{navigate('/')}}/>
				</Navbar.Brand>
				<Navbar.Toggle aria-controls="responsive-navbar-nav"/>
				<Navbar.Collapse id="responsive-navbar-nav">
					<Nav>
						<Nav.Link className='' onClick={()=>{navigate("/")}}>Search</Nav.Link>
						{
							userData.userRights <= 1
							? <Nav.Link className='' onClick={()=>{navigate("/add")}}>Add picture</Nav.Link>
							: null
						}
						{
							userData.userRights <= 1
							? <Nav.Link onClick={()=>{navigate("/my_pictures")}}>My pictures</Nav.Link>
							: null
						}
					</Nav>
				</Navbar.Collapse>
				<Navbar.Collapse className="justify-content-end">
					<Nav>
						{
							userData.userRights == 0
							?
							<Nav.Link className='mx-2' onClick={()=>{navigate("/administration")}}>
								Administration
							</Nav.Link>
							: 
							null
						}
						{
							userData.userName == ''
							? <Button variant="secondary" href="/login">Log In</Button>
							: <Nav>
								<Navbar.Text> 
									{userData.userName}
								</Navbar.Text>
								<Image roundedCircle src={userData.iconSrc} className = "navbar-icon mx-2"/>
							  	<Button variant="secondary" 
								  onClick={logOut}>
							  		Log Out
							  	</Button>
							  </Nav>
						}
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		</div>
	);
}

export default NaviBar;