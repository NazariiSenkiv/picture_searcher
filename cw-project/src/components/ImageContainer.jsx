import React, { useState } from "react";
import { Badge, Button, Modal, Form, Image } from "react-bootstrap";

import styles from '../components/ImageContainer.module.css'

const ImageContainer = function({imageInfo}) {
    
    const edit = function(e) {
        setShowModal(true);
    }

    const cancelChanges = function() {
        setName(imageInfo.name);
        setTags(imageInfo.tags);
        setSrc(imageInfo.src);

        setShowModal(false);
    }

    const backendURL = 'http://cw-backend-server/'

    const saveChanges = function() {
        var formData = new FormData();
        formData.append('id', imageInfo.id);
        formData.append('name', name);
        formData.append('tags', tags);
        formData.append('src', src);

        fetch(backendURL+"updatePictureData.php", {
            method: 'POST',
            header: {"Content-Type": 'application/x-www-form-urlencoded'},
            body: formData
        })
        .then(response=>response.json())
        .then(response=>{
            if(response.updated == true) {
                setShowModal(false);
            }
        });
    }

    const [removed, setRemoved] = useState(false);

    const remove = function(e) {

        var formData = new FormData();
        formData.append('id', imageInfo.id);

        fetch(backendURL+"removeImage.php", {
            method: 'POST',
            header: {"Content-Type": 'application/x-www-form-urlencoded'},
            body: formData
        })
        .then(response=>response.json())
        .then(response=>{
            if(response.removed == true) {
                setRemoved(true);
            }
        });
    }

    const [showModal, setShowModal] = useState(false);

    const [name, setName] = useState(imageInfo.name);
    const [tags, setTags] = useState(imageInfo.tags);
    const [src, setSrc] = useState(imageInfo.src);

    return (
        <div className={styles.image_container}>
            {
                removed 
                ? 
                <div>
                    <h1 className={styles.removed_header}>{name}</h1>
                    <h1 className={styles.header}>(Removed)</h1>
                </div>
                : <h1 className={styles.header}>{name}</h1>
            }
            {
                imageInfo.author_id != JSON.parse(localStorage.getItem('userId'))
                ?
                <div>
                    <Image roundedCircle src={imageInfo.author_icon} className={styles.icon}/>
                    <div className={styles.author_caption}>{imageInfo.author_name}</div>
                </div>
                :
                <div>
                    <Image roundedCircle src={JSON.parse(localStorage.getItem('iconSrc'))} 
                    className={styles.icon}
                    />
                    <div className={styles.author_caption}>You</div>
                </div>
            }
            <img src={src} className={styles.image}/>
            {
                tags.split(' ').map(tag =>
                    <Badge bg="secondary" className="mx-1">{tag}</Badge>
                )
            }
            {
                imageInfo.author_id == JSON.parse(localStorage.getItem('userId'))
                || JSON.parse(localStorage.getItem('userRights')) == 0
                ? 
                <div className="d-flex justify-content-end mx-2 my-0">
                    <Button variant="secondary" className="mx-1" onClick={(e)=>{edit()}}>Edit</Button>
                    <Button variant="danger" className="mx-1" onClick={(e)=>{remove()}}>Remove</Button>
                </div>
                : null
            }
            {
                showModal ?
                <Modal show={showModal} onHide={()=>{setShowModal(false)}}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group className="mb-3">
                                <Form.Label>Picture name</Form.Label>
                                <Form.Control type="text" value={name} 
                                onChange={(e)=>{setName(e.currentTarget.value)}}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Tags</Form.Label>
                                <Form.Control type="text" value={tags}
                                onChange={(e)=>{setTags(e.currentTarget.value)}}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Source URL</Form.Label>
                                <Form.Control type="text" value={src}
                                onChange={(e)=>{setSrc(e.currentTarget.value)}}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Image src={src} className={styles.image}/>
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={cancelChanges}>
                            Cancel
                        </Button>
                        <Button variant="primary" onClick={saveChanges}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal>
                : null
            }
        </div>
    );
}

export default ImageContainer;