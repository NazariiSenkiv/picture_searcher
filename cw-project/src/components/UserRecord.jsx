import {Form, Button, Modal } from 'react-bootstrap'
import { useState } from 'react';

const backendPageURL = 'http://cw-backend-server/updateUser.php';

const UserRecord = function({user}) {

    const applyChanges = function() {
        var formData = new FormData();
        formData.append('id', id);
        formData.append('name', name);
        formData.append('password', password);
        formData.append('icon_src', src);
        formData.append('access_rights', rights);

        fetch(backendPageURL, {
            method: 'POST',
            header: {"Content-Type": 'application/x-www-form-urlencoded'},
            body: formData
        })
        .then(response=>response.json())
        .then(response=>{
            if(response.updated == true) {
                setShowModal(true);
            }
        });
    }

    const [id, setId] = useState(user.id);
    const [name, setName] = useState(user.name);
    const [password, setPassword] = useState(user.password);
    const [src, setSrc] = useState(user.icon_src);
    const [rights, setRights] = useState(user.access_rights);

    const [showModal, setShowModal] = useState(false);

    return (
        <div>
            <Form>
                <span className="record-element mx-1">
                    <Form.Label className='px-2 mb-0 mt-2'>Id</Form.Label>
                    <Form.Control type="text" value={id} disabled/>    	
                </span>	
                <span className="record-element mx-1">
                    <Form.Label className='px-2 mb-0 mt-2'>Name</Form.Label>
                    <Form.Control type="text" value={name} 
                    onChange={(e)=>{setName(e.currentTarget.value)}}
                    />  
                </span>	
                <span className="record-element mx-1" >
                    <Form.Label className='px-2 mb-0 mt-2'>Password</Form.Label>
                    <Form.Control type="text" value={password}
                    onChange={(e)=>{setPassword(e.currentTarget.value)}}
                    />  
                </span>	
                <span className="record-element mx-1" >
                    <Form.Label className='px-2 mb-0 mt-2'>Icon Src</Form.Label>
                    <Form.Control type="text" value={src}
                    onChange={(e)=>{setSrc(e.currentTarget.value)}}
                    />  
                </span>	
                <span className="record-element mx-1">
                    <Form.Label className='px-2 mb-0 mt-2'>Rights</Form.Label>
                    <Form.Control type="text" value={rights}
                    onChange={(e)=>{setRights(e.currentTarget.value)}}
                    />  
                </span>	
                <span>
                    <Button variant="secondary" className='apply-button' onClick={applyChanges}> Apply </Button>  
                </span>	
            </Form>
            {
            showModal ?
            <div>
                <Modal show={showModal} onHide={(e)=>{setShowModal(false);}}>
                <Modal.Header closeButton>
                    <Modal.Title>Info</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Applied successfully.</p>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={(e)=>{setShowModal(false);}}>
                        Ok
                    </Button>
                </Modal.Footer>
                </Modal>
            </div>
            : null   
            }
        </div>
    );
}

export default UserRecord;