import React from "react";
import { useNavigate } from "react-router-dom";

const SearchBar = function() {

    return (
        <form action="/search">
            <div className="d-flex justify-content-center">
                <input type="text" name="q" placeholder="Search.." className="search-bar"/>
            </div>
        </form>
    );
}

export default SearchBar;