<?php
    header("Access-Control-Allow-Origin: http://localhost:3000");
    include "includes/db.php";

    $user_id = $_GET['id'];

    $db_answer = mysqli_query($connection, "SELECT * FROM `images` WHERE `author_id`='$user_id'");

    if ($db_answer == false) {
        echo 'Database error';
        exit();
    }

    $records_count = mysqli_num_rows($db_answer);
    $images = array();

    if ($records_count > 0) {
        while($record = mysqli_fetch_assoc($db_answer)) {
            array_push($images, $record);
        }
    }
    else {
        echo 'no results';
        exit();
    }

    echo json_encode($images);
?>