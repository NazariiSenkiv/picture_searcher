<?php
    header("Access-Control-Allow-Origin: http://localhost:3000");
    include "includes/db.php";

    $tags = $_GET['q'];

    $tags = trim($tags);
    $tags = explode(' ', $tags);

    $query_string = 'SELECT * FROM `images` WHERE';

    foreach ($tags as $tag) {
        $query_string .= " `tags` LIKE '%$tag%' OR";
    }
    // delete last odd OR
    $query_string = substr($query_string, 0, strlen($query_string)-3);

    $db_answer = mysqli_query($connection, $query_string);

    if ($db_answer == false) {
        echo 'Database error';
        exit();
    }
    $records_count = mysqli_num_rows($db_answer);

    $images = array();

    if ($records_count > 0) {

        while($record = mysqli_fetch_assoc($db_answer)) {
           
            $user_id = $record['author_id'];
            $users_db_answer = mysqli_query($connection, 
            "SELECT `name` AS `author_name`, `icon_src` AS `author_icon` FROM `users` WHERE `id`='$user_id'");
            $user_record = mysqli_fetch_assoc($users_db_answer);

            $record = array_merge($record, $user_record);
            array_push($images, $record);
        }
    }
    else {
        echo 'no results';
    }

    echo json_encode($images);
?>