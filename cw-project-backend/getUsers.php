<?php
    header("Access-Control-Allow-Origin: http://localhost:3000");
    include "includes/db.php";

    $db_answer = mysqli_query($connection, "SELECT * FROM `users`");

    if($db_answer == false) {
        echo json_encode(array('success'=>false));
        exit();
    }

    $records_count = mysqli_num_rows($db_answer);
    $users = array();

    if ($records_count > 0) {
        while($record = mysqli_fetch_assoc($db_answer)) {
            array_push($users, $record);
        }
    }
    else {
        echo json_encode(array('success'=>false));
        exit();
    }

    echo json_encode($users);
?>