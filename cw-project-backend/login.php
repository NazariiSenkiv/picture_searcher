<?php
    header("Access-Control-Allow-Origin: http://localhost:3000");
    include "includes/db.php";
    
    $name = $_POST['name'];
    $pass = $_POST['password'];

    $db_answer = mysqli_query($connection, "SELECT * FROM `users` WHERE `name` = '$name'");

    if ($db_answer == false) {
        echo 'Database error';
        exit();
    }

    $record = mysqli_fetch_assoc($db_answer);

    $response = array(
        'login_correct' => true,
        'password_correct' => true,
        'image_src' => '',
        'user_id' => 0,
        'rights' => 2
    );

    if ($record != null) {
        if ($record['password'] == $pass) {
            $response['image_src'] = $record['icon_src'];
            $response['user_id'] = $record['id'];
            $response['rights'] = $record['access_rights'];
        }
        else {
            $response['password_correct'] = false;
        }
    }
    else {
        $response['login_correct'] = false;
    }

    echo json_encode($response);
?>